const session = require("express-session");
const userData = require("../../../models/");
const { validator } = require("./validators");

// ----------------------------------------------------------------Authenticate-----------------------------------------------

const authenticate = async(req, res, next) => {
    let token = req.headers.token;

    // -----------------------Checking Session-------------------------------
    if (token == undefined) {
        return res.send(messages.expired("[Please Log-in First],Session is"));
    }

    // -------------------------------Decoding Token---------------------

    let base64Url = token.split(".")[1];
    let base64 = JSON.parse(atob(base64Url));

    // ------------------------------Finding Relevent Data in Database---------------------
    try {
        var userInfo = await userData.findOne({ email: base64.email });
    } catch (error) {
        console.log(erroe)
        return res.send(messages.not_found("User Data"))
    }


    // ------------------------------------Comparing Data in Session----------------
    // console.log(`--------------------
    // ${userInfo.email} == ${req.session.email}
    // ${userInfo.uType} == ${req.session.uType}
    //         -------------------- `);

    if (
        userInfo.email != req.session.email ||
        userInfo.uType != req.session.uType ||
        base64.uType != "admin" ||
        base64.uType == "user"
    ) {
        return res.send(messages.unauthorized(""));
    }


    next();
};

module.exports = authenticate;