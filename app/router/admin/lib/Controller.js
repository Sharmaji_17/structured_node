const controller = {};
const user = require("../../../models/");

controller.renderAll = async(req, res) => {
    try {
        const Newuser = await user.aggregate([{
                $match: {
                    uType: "user",
                },
            },
            {
                $project: {
                    uType: 0,
                    _id: 0,
                    __v: 0,
                    password: 0,
                },
            },
        ]);
        console.log(Newuser);
        res.send(Newuser);
    } catch (error) {
        console.log(error);
        res.send(error);
    }
};

module.exports = { controller };