const express = require("express");
const session = require("express-session");
const bodyParser = require("body-parser");
const routes = require("./route");

function connection() {}

connection.prototype.initialize = () => {
    this.app = express();
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json({ extended: true }));
    this.app.use(
        session({
            secret: "secret-key",
            resave: false,
            saveUninitialized: false,
            cookie: {
                expires: 600000,
            },
        })
    );
    this.app.listen(3000, () => {
        this.app.use("/api/v1/", routes);
        console.log("Server Started");
    });
};

module.exports = new connection();