const controller = {};
const user = require("../../../models/");

controller.update = async(req, res) => {
    if (req.body.password != undefined || req.body._id != undefined) {
        return res.status(400).send("UnAuthorized Update");
    }

    try {
        const Newuser = await user.findOneAndUpdate({ email: req.session.email },
            req.body
        );
        console.log(Newuser);
        return res.send(messages.successfully("Updated"));
    } catch (error) {
        console.log(error);
        return res.staus(500).send("Serverside Error");
    }
};

controller.render = async(req, res) => {
    try {
        const Newuser = await user.aggregate([{
                $match: {
                    email: req.session.email,
                },
            },
            {
                $project: {
                    uType: 0,
                    _id: 0,
                    __v: 0,
                    password: 0,
                },
            },
        ]);
        console.log("Session", req.session);
        console.log("Final Db", Newuser);
        // return res.send(messages.successfully("Updated"));
        res.json({ status: messages.successfully(""), YourProfile: Newuser });
    } catch (error) {
        console.log(error);
        res.send(error);
    }
};

module.exports = { controller };